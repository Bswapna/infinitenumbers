class InfiniteNumber:

	# default constructor
	def __init__(self, optional_array=[], optional_string=""):
		self.digits = []

		if len(optional_array) > 0:
			# optional parameter has been provided
			self.digits = optional_array
		elif len(optional_string) != 0:
			length = len(optional_string)
			for index in range(length):
				self.digits.append(int(optional_string[length - 1 - index]))
		else:
			# read user input, no optional parameter provided
			while True:
				user_input = input(
					"Enter a digit and press enter:" 
					if len(self.digits) == 0 
					else "Enter another number or press enter to end input.")
				print(user_input)
				
				try:
					if len(user_input) == 1:
						self.digits.append(int(user_input[0]))
					elif len(user_input) > 1:
						print("You have not entered a digit, try again.")
						pass
					else:
						if len(self.digits) == 0:
							print("Insufficient digits, please try again.")
						else:
							break
				except:
					print("You have not entered a digit, try again.")
					pass
		
	def display(self):
		print_string = ""
		for i in range(len(self.digits)):
			print_string += str(self.digits[i])
		print(print_string)

	def add(self, second_num):
		# finish this function to return a InfiniteNumber by adding self to second_num
		# return self.convert_to_list(self.convert_to_int(self.digits) + self.convert_to_int(second_num))
			def convert_to_int(input):
				''' This function is used to convert the given input to integer
                Arguments:
                     input : list of integers
                Returns:
                     returns the integer'''
				return int("".join(map(str, input)))

			def convert_to_list(integer):
				''' This function is used to convert the given input to integer
									Arguments:
											input : integer
									Returns:
											returns the list of integers'''
				return [int(x) for x in str(integer)]
			return convert_to_list(convert_to_int(self.digits) + convert_to_int(second_num))
			pass

	def increment(self):
		# finish this function to return a InfiniteNumber by incrementing the current number
		# you can use the add() function if you want
		a = self.digits
		n = len(a)
		#Add 1 to last digit and find carry
		a[n-1] += 1
		carry = a[n-1]/10
		a[n-1] = a[n-1] % 10
		# Traverse from second last digit
		for i in range(n-2, -1, -1):
			if (carry == 1):
				a[i] += 1
				carry = a[i]/10
				a[i] = a[i] % 10
		# If carry is 1, we need to add a 1 at the beginning of list
		if (carry == 1):
			a.insert(0, 1)
		x = []
		for i in range(len(a)):
			x.append(a[i])
		return x
		pass

	def compare(self, second_num):
		a = self.digits
		b = second_num

		# finish this function to 
		# 	return 	1	if second_num is smaller than self
		#		return -1 if second_num is larger than self
		#		return 	0	if second number is equal to self
		if len(a) > len(b):
			return 1
		elif len(b) > len(a):
			return -1
		else:
			for index in range(len(a)):
				if a[index] > b[index]:
					return 1
				elif b[index] > a[index]:
					return -1
			return 0
		pass

	def mul(self, second_num):
		# finish this function to return a InfiniteNumber by multiplying self with
		#	the current number.
		# you can use the add(), increment() functions if you want
		def convert_to_int(input):
			return int("".join(map(str, input)))

		def convert_to_list(integer):
			return [int(x) for x in str(integer)]
		return convert_to_list(convert_to_int(self.digits) * convert_to_int(second_num))
		pass

	def pow(self, second_num):
		# finish this function to return a InfiniteNumber
		# you can use the mul(), increment() functions if you want
		def convert_to_int(input):
			return int("".join(map(str, input)))
		def convert_to_list(integer):
			return [int(x) for x in str(integer)]
		res = 1
		num1 = convert_to_int(self.digits)
		num2 = convert_to_int(second_num)
		for i in range(num2):
			res *= num1
		return convert_to_list(res)
		pass

num1 = InfiniteNumber([0,0,1])
num2 = InfiniteNumber([4,5,6])
print(num1.digits)
print(num2.digits)
add_numbers = num1.add(num2.digits)
increment_number1 = num1.increment()
increment_number2 = num2.increment()
compare_number = num1.compare(num2.digits)
multiply_number = num1.mul(num2.digits)
power_of_number = num1.pow(num2.digits)
print(add_numbers)
print(increment_number1)
print(increment_number2)
print(compare_number)
print(multiply_number)
print(power_of_number)
